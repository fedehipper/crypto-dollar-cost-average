import '../css/styles.css';
import { RiFileCopyLine } from "react-icons/ri";

function ModalConfirmation(props) {

    const copiarAlPortaPapeles = () => {
        navigator.clipboard.writeText(props.wallet.direccion).then(function () {
            alert('the wallet has been copied successfully!!');
        }, function (err) {
            alert('An error occurred when trying to copy the url, please try again.')
        });
    }

    return <div id="modalConfirmacion" className="modal" tabIndex="-1">
        <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title">Dontations</h5>
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div className="modal-body">
                    <p>My {props.wallet.criptomoneda} wallet:</p>
                    <h6 className='texto-salto-linea'>{props.wallet.direccion}
                        <span title='copy wallet' className='mt-2 text-primary clickeable' onClick={copiarAlPortaPapeles}>&nbsp;&nbsp;<RiFileCopyLine /></span>
                    </h6>
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-primary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
}

export default ModalConfirmation;