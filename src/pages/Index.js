import React, { useEffect, useState } from 'react';
import '../css/styles.css';
import { IoMdAdd, IoMdRemove } from "react-icons/io";
import ModalConfirmation from './modal-confirmation';

function Index() {

    const [transacciones, setTransacciones] = useState({
        1: {
            cotizacion: null,
            monto: null
        },
        2: {
            cotizacion: null,
            monto: null
        },
        3: {
            cotizacion: null,
            monto: null
        },
        4: {
            cotizacion: null,
            monto: null
        }
    });
    const [dollarCostAverage, setDollarCostAverage] = useState('');
    const [transaccionesCargadasPorCompleto, setTransaccionesCargadasPorCompleto] = useState(true);
    const [cargaInicial, setCargaInicial] = useState(true);
    const [removerTransaccionDisabled, setRemoverTransaccionDisabled] = useState(true);
    const [walletSeleccionada, setWalletSeleccionada] = useState('');
    const BTC_WALLET = '3KUAkrTFqcSRzjvPq2fHT2zXNcmNFjY5S3';
    const ADA_WALLET = 'addr1q8cnh52shwy0njw6u6swhdl6shzvv47stt3yvde04aj3azy0eqqqfgcgq3jf82hy2yef5tcg3m4vqy0xlmgajrr0gdkq3rrw40';

    useEffect(() => {
        let url = new URL(window.location.href);
        let parametro = url.searchParams.get('data');
        if (parametro && cargaInicial) {
            cargarTransaccionesPorParametro(parametro);
            setCargaInicial(false);
        }
    }, [cargaInicial]);

    useEffect(() => {
        verificarDisabledQuitarTransaccion();
    }, [transacciones]);

    const cargarTransaccionesPorParametro = (parametro) => setTransacciones(JSON.parse(atob(parametro)));

    const asignarMonto = (evento, orden) => {
        let transaccionesBackUp = {}
        Object.assign(transaccionesBackUp, transacciones);
        transaccionesBackUp[orden].monto = evento.target.value;
        setTransacciones(transaccionesBackUp);
        agregarParametroEncriptarABase64(transaccionesBackUp);
    }

    const asignarCotizacion = (evento, orden) => {
        let transaccionesBackUp = {}
        Object.assign(transaccionesBackUp, transacciones);
        transaccionesBackUp[orden].cotizacion = evento.target.value;
        setTransacciones(transaccionesBackUp);
        agregarParametroEncriptarABase64(transaccionesBackUp);
    }

    const agregarParametroEncriptarABase64 = (transacciones) => window.history.replaceState({}, null, origin + '?data=' + btoa(JSON.stringify(transacciones)));

    const agregarTransaccion = () => {
        let transaccionesBackUp = {}
        Object.assign(transaccionesBackUp, transacciones);

        const nuevaTransaccion = {};
        nuevaTransaccion[cantidadTransacciones() + 1] = { cotizacion: null, monto: null };
        setTransacciones({});
        Object.assign(transaccionesBackUp, nuevaTransaccion);
        setTransacciones(transaccionesBackUp);
    }

    const cantidadTransacciones = () => Object.keys(transacciones).length;

    const quitarTransaccion = () => {
        if (cantidadTransacciones() > 1) {
            let transaccionesBackUp = {}
            Object.assign(transaccionesBackUp, transacciones);

            delete transaccionesBackUp[cantidadTransacciones()];
            setTransacciones(transaccionesBackUp);
            agregarParametroEncriptarABase64(transaccionesBackUp);
        }
    }

    const transaccionesCompletamenteCargadas = () => {
        return Object.keys(transacciones)
            .map(orden => !!transacciones[orden].cotizacion && !!transacciones[orden].monto)
            .reduce((unParCargado, otroParCargado) => unParCargado && otroParCargado, true);
    }

    const calcularDCA = () => {
        if (transaccionesCompletamenteCargadas()) {
            setTransaccionesCargadasPorCompleto(true);
            const pesosSumados = Object
                .keys(transacciones)
                .map(orden => transacciones[orden].monto * transacciones[orden].cotizacion)
                .reduce((unPeso, otroPeso) => unPeso + otroPeso, 0);

            const montosSumados = Object
                .keys(transacciones)
                .map(orden => parseInt(transacciones[orden].monto))
                .reduce((unPeso, otroPeso) => unPeso + otroPeso, 0);

            setDollarCostAverage((pesosSumados / montosSumados).toFixed(2));
        } else {
            setTransaccionesCargadasPorCompleto(false);
        }
    }

    const copiarUrl = () => {
        navigator.clipboard.writeText(window.location.href).then(function () {
            alert('the url: ' + window.location.href + 'has been copied successfully!!');
        }, function (err) {
            alert('An error occurred when trying to copy the url, please try again.')
        });
    }

    const verificarDisabledQuitarTransaccion = () => cantidadTransacciones() > 1 ? setRemoverTransaccionDisabled(null) : setRemoverTransaccionDisabled(true);

    const mostrarWalletBitcoin = () => setWalletSeleccionada({ criptomoneda: 'btc', direccion: BTC_WALLET });

    const mostrarWalletCardano = () => setWalletSeleccionada({ criptomoneda: 'ada', direccion: ADA_WALLET });

    return <>
        <div className='container col-md-7 col fonts'>
            <h2 className='pt-3 mb-4 text-center'>Crypto Dollar-Cost Averaging</h2>

            <p>To calculate the DCA (Dollar Cost Averaging), enter the pairs (cost, quote) of the cryptocurrency from the beginning.</p>

            <div className='card mb-3'>
                <div className='scroll-input-transacciones mb-3'>
                    <div className='ms-3 mt-3 me-2'>For a more approximate result, For each <strong>COST</strong>, enter the <strong>QUOTE</strong> price at the exact moment of the purchase. Enter your transactions below:</div>

                    {Object.keys(transacciones).map(orden => {
                        return <div className='mt-3 d-flex justify-content-center' key={orden}>
                            <div className='ps-3 d-flex flex-column-reverse justify-content-center'>
                                <span>{orden}</span>
                            </div>
                            <div className='col ps-3 pe-3'>
                                <input
                                    value={transacciones[orden].monto ? transacciones[orden].monto : ''}
                                    type='number'
                                    className='form-control'
                                    onChange={evento => asignarMonto(evento, orden)}
                                    placeholder='Cost'
                                />
                            </div>
                            <div className='d-flex flex-column-reverse justify-content-center'>
                                <span><strong>-</strong></span>
                            </div>
                            <div className='col ps-3 pe-3'>
                                <input
                                    value={transacciones[orden].cotizacion ? transacciones[orden].cotizacion : ''}
                                    type='number'
                                    className='form-control'
                                    onChange={evento => asignarCotizacion(evento, orden)}
                                    placeholder='Quote'
                                />
                            </div>
                        </div>
                    })}
                </div>

                <div className='card card-footer pt-3 pb-0 pe-0 ps-0'>
                    <div className='d-flex justify-content-between ms-3 me-3 mb-3'>
                        <button
                            className='btn btn-secondary boton-mobile-accion'
                            title='Add transaction'
                            onClick={agregarTransaccion}>&nbsp;&nbsp;<IoMdAdd />&nbsp;&nbsp;
                        </button>
                        <button
                            title='Remove transaction'
                            className={`btn btn-secondary boton-mobile-accion ms-3 ${removerTransaccionDisabled ? 'deshabilitado' : ''}`}
                            disabled={removerTransaccionDisabled}
                            onClick={quitarTransaccion}>&nbsp;&nbsp;<IoMdRemove />&nbsp;&nbsp;
                        </button>
                    </div>
                </div>
            </div>

            <div className='card p-3 mb-3'>
                <div>
                    <div className='mb-3 d-flex justify-content-start'>
                        <button className="btn btn-primary boton-mobile-accion me-3" onClick={calcularDCA}>Calculate</button>
                        <button className='btn btn-success boton-mobile-accion' onClick={copiarUrl}>Save URL</button>
                    </div>

                    {transaccionesCargadasPorCompleto &&
                        <h3 className='mb-0'>DCA: {dollarCostAverage ? `USD ${dollarCostAverage}` : '---'}</h3>
                    }
                    {!transaccionesCargadasPorCompleto &&
                        <h5 className='text-danger mb-0'>You missed fields to enter.</h5>
                    }
                </div>
            </div>

            <div className='card p-3 mb-3'>
                <h5 className='mb-3 text-center'>Would you like to make a donation to me? 💌</h5>
                <div>
                    <div className='d-flex justify-content-center'>
                        <div className='btn me-2 boton-donacion' onClick={mostrarWalletBitcoin} data-bs-toggle="modal" data-bs-target="#modalConfirmacion">
                            <img src="https://assets.coingecko.com/coins/images/1/large/bitcoin.png?1547033579" width="30" />
                        </div>
                        <div className='btn ms-2 boton-donacion' onClick={mostrarWalletCardano} data-bs-toggle="modal" data-bs-target="#modalConfirmacion">
                            <img src="https://assets.coingecko.com/coins/images/975/large/cardano.png?1547034860" width="30" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <ModalConfirmation wallet={walletSeleccionada} />
    </>

}

export default Index;