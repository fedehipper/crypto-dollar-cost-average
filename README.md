Para deployar en firebase seguir los pasos:

1. En firebase crear un proyecto.

2. En la sección hosting copiar el sdk.

3. Tener ese sdk en la carpeta environments.

4. instalar firebase con npm.

5. instalar globalalmente firebase-tools:
npm install -g firebase-tools  

6. firebase login

7. firebase init
En las preguntas que firebase hace, no reemplazar ningún archivo public (index.js) etc..

8. firebase deploy

9. npm run build y relanzar el deploy con los 
archivos resultantes del build, copiarlos a public.

10. una vez deployado volver a dejar el public para pruebas, copiar el contenido de public-backup-tests en public.